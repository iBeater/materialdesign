package com.example.amartineza.materialdesing;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    String userEmail;
    String userPass;
    EditText etUserEmail, etUserPass;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUserEmail = (EditText) findViewById(R.id.et_email_address);
        etUserPass = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()) {
                    userEmail = etUserEmail.getText().toString();
                    userPass = etUserPass.getText().toString();
                    User user = new User(userEmail, userPass);
                    SharedPreferencesManager.saveUserData(MainActivity.this, user);

                    Intent intent = new Intent(MainActivity.this, MostrarDatosActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        boolean isDateSave = SharedPreferencesManager.isUserDataSave(this);
        if (isDateSave) {
            Intent intent = new Intent(this, MostrarDatosActivity.class);
            startActivity(intent);
            finish();
        }

    }

    public Boolean validateForm() {
        Boolean itsValid = true;
        if (etUserEmail.getText().toString().isEmpty()) {
            etUserEmail.setError("Introduce el Correo");
            itsValid = false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etUserEmail.getText().toString()).matches()) {
            etUserEmail.setError("Introduce el Correo Valido");
            itsValid = false;
        }
        if (etUserPass.getText().toString().isEmpty()) {
            etUserPass.setError("Introduce la Contraseña");
            itsValid = false;
        }
        return itsValid;
    }
}
