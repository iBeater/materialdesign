package com.example.amartineza.materialdesing;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by amartineza on 3/6/2018.
 */

public class SharedPreferencesManager {
    private static final String GLOBAL_PREFERENCES = "GLOBAL_PREFERENCES";
    private static SharedPreferences sharedPreferences;

    public SharedPreferencesManager() {
    }

    private static String getPreferencesKey(Context context, String key) {
        return String.format("%s.%s", context.getPackageName(), key); //obtener un nombre unico de archivo
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(getPreferencesKey(context, GLOBAL_PREFERENCES), Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    static User getUserData(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        String email = sharedPreferences.getString("email", "");
        String pass = sharedPreferences.getString("pass", "");
        return new User(email, pass);
    }

    static void saveUserData(Context context, User user) { //escribo todos los datos en el shared
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email", user.getUserEmail());
        editor.putString("pass", user.getUserPass());
        editor.putBoolean("is_user_data_save", true);
        editor.apply();
    }

    static void clearUserData(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    static boolean isUserDataSave(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getBoolean("is_user_data_save", false);
    }

}
