package com.example.amartineza.materialdesing;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MostrarDatosActivity extends AppCompatActivity {

    TextView tvUserEmail, tvUserPass;
    Button btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_datos);
        tvUserEmail = (TextView)findViewById(R.id.tv_user_email);
        tvUserPass = (TextView)findViewById(R.id.tv_user_password);
        btnLogOut = (Button)findViewById(R.id.btn_log_out);

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferencesManager.clearUserData(MostrarDatosActivity.this);
                Intent intent = new Intent(MostrarDatosActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        User user = SharedPreferencesManager.getUserData(this);
        tvUserEmail.setText(user.getUserEmail());
        tvUserPass.setText(user.getUserPass());
    }
}
